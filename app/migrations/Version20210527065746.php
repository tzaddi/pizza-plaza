<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210527065746 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_items ADD orderitems_order_id INT NOT NULL');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_62809DB0F8D7D50A FOREIGN KEY (orderitems_order_id) REFERENCES `order` (id)');
        $this->addSql('CREATE INDEX IDX_62809DB0F8D7D50A ON order_items (orderitems_order_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_items DROP FOREIGN KEY FK_62809DB0F8D7D50A');
        $this->addSql('DROP INDEX IDX_62809DB0F8D7D50A ON order_items');
        $this->addSql('ALTER TABLE order_items DROP orderitems_order_id');
    }
}
