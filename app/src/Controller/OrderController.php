<?php

namespace App\Controller;

use App\Entity\Pizza;
use App\Repository\PizzaRepository;

use App\Entity\TempOrder;
use App\Repository\TempOrderRepository;

use App\Repository\PizzaOfTheDayRepository;

use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\OrderItems;

use App\Form\CustomerType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("/order")
 */
class OrderController extends AbstractController
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="order_index", methods={"GET"})
     */
    public function index(PizzaRepository $PizzaRepository, PizzaOfTheDayRepository $PizzaOfTheDayRepository): Response
    {
        if (!$this->session->get('order_sess_id')) {
            $this->session->set('order_sess_id', uniqid());
        }

        $sale = $PizzaOfTheDayRepository->findOneByToday();
        $pizzaId =  $sale->getPizzaId();
        $salePrice = $sale->getPrice();

        return $this->render('order/index.html.twig', [
            'pizzas' => $PizzaRepository->findAll(),
            'sale_pizza_id' => $pizzaId,
            'sale_pizza_price' => $salePrice
        ]);
    }

    /**
     * @Route("/add", name="order_add", methods={"POST"})
     */
    public function add(
        PizzaRepository $PizzaRepository,
        Request $request,
        TempOrderRepository $TempOrderRepository,
        PizzaOfTheDayRepository $PizzaOfTheDayRepository
    ): Response {
        //check if the pizza is already added
        $check_pizza = $TempOrderRepository->findOneBy(
            array(
                "sess_id" => $this->session->get('order_sess_id'),
                'pizza_id' => $request->get("pizza_id")
            )
        );

        if ($check_pizza != NULL) {
            $id = $check_pizza->getId();
            $current_qty = $check_pizza->getQuantity();
            $new_qty =  $current_qty + $request->get("quantity");

            $TempOrder = new TempOrder();
            $TempOrder = $this->getDoctrine()->getRepository(TempOrder::class)->find($id);
            $TempOrder->setQuantity($new_qty);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('order_show');
        }

        //check if pizza of the day
        $sale = $PizzaOfTheDayRepository->findOneByToday();
        $salePizzaId =  $sale->getPizzaId();
        $salePrice = $sale->getPrice();

        $pizza = $PizzaRepository->find($request->get("pizza_id"));

        if ($request->get("pizza_id") == $salePizzaId) {
            $pizzaPrice = $salePrice;
        } else {
            $pizzaPrice = $pizza->getPrice();
        }

        $TempOrder = new TempOrder();
        $TempOrder->setSessId($this->session->get('order_sess_id'));
        $TempOrder->setPizzaId($request->get("pizza_id"));
        $TempOrder->setQuantity($request->get("quantity"));
        $TempOrder->setPrice($pizzaPrice);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($TempOrder);
        $entityManager->flush();

        return $this->redirectToRoute('order_show');
    }

    /**
     * @Route("/update", name="order_update", methods={"POST"})
     */
    public function update(TempOrderRepository $TempOrderRepository, Request $request): Response
    {
        $check_pizza = $TempOrderRepository->findOneBy(
            array(
                "sess_id" => $this->session->get('order_sess_id'),
                'pizza_id' => $request->get("pizza_id")
            )
        );
        $id = $check_pizza->getId();

        if ($request->get("quantity") == 0) {
            $TempOrder = new TempOrder();
            $TempOrder = $this->getDoctrine()->getRepository(TempOrder::class)->find($id);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($TempOrder);
            $entityManager->flush();
            return $this->redirectToRoute('order_show');
        }

        $TempOrder = new TempOrder();
        $TempOrder = $this->getDoctrine()->getRepository(TempOrder::class)->find($id);
        $TempOrder->setQuantity($request->get("quantity"));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return $this->redirectToRoute('order_show');
    }

    /**
     * @Route("/add_sale", name="order_add_sale", methods={"POST"})
     */
    public function add_sale(PizzaOfTheDayRepository $PizzaOfTheDayRepository, Request $request): Response
    {
        $sale = $PizzaOfTheDayRepository->findOneByToday();
        $salePrice = $sale->getPrice();

        $TempOrder = new TempOrder();
        $TempOrder->setSessId($this->session->get('order_sess_id'));
        $TempOrder->setPizzaId($request->get("pizza_id"));
        $TempOrder->setQuantity($request->get("quantity"));
        $TempOrder->setPrice($salePrice);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($TempOrder);
        $entityManager->flush();

        return $this->redirectToRoute('order_show');
    }

    /**
     * @Route("/show", name="order_show", methods={"GET"})
     */
    public function show(TempOrderRepository $TempOrderRepository, PizzaRepository $PizzaRepository): Response
    {
        $pizzas = $PizzaRepository->findAll();
        $orders = $TempOrderRepository->findBy(
            array("sess_id" => $this->session->get('order_sess_id'))
        );

        return $this->render('order/show.html.twig', [
            'orders' => $orders,
            'pizzas' => $pizzas
        ]);
    }

    /**
     * @Route("/checkout", name="order_checkout", methods={"GET"})
     */
    public function checkout(TempOrderRepository $TempOrderRepository, PizzaRepository $PizzaRepository): Response
    {
        $pizzas = $PizzaRepository->findAll();
        $orders = $TempOrderRepository->findBy(
            array("sess_id" => $this->session->get('order_sess_id'))
        );

        return $this->render('order/checkout.html.twig', [
            'orders' => $orders,
            'pizzas' => $pizzas
        ]);
    }

    /**
     * @Route("/customer", name="order_customer", methods={"GET","POST"})
     */
    public function customer(
        TempOrderRepository $TempOrderRepository,
        PizzaRepository $PizzaRepository,
        Request $request
    ): Response {
        $customer = new Customer();
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($customer);
            $entityManager->flush();

            $entityManager = $this->getDoctrine()->getManager();
            $Order = new Order();
            $Order->setCustomer($customer);
            $Order->setTimestamp(new \DateTime());
            $entityManager->persist($Order);
            $entityManager->flush();

            $tmpOrders = $TempOrderRepository->findBy(
                array("sess_id" => $this->session->get('order_sess_id'))
            );
            foreach ($tmpOrders as $tmpOrder) {
                $pizza = $PizzaRepository->find($tmpOrder->getPizzaId());

                $entityManager = $this->getDoctrine()->getManager();
                $OrderItems = new OrderItems();
                $OrderItems->setQuantity($tmpOrder->getQuantity());
                $OrderItems->setPizzaId($tmpOrder->getPizzaId());
                $OrderItems->setPizza($pizza);
                $OrderItems->setPrice($tmpOrder->getPrice());
                $OrderItems->setOrderId($Order->getId());
                $OrderItems->setOrderitemsOrder($Order);
                $entityManager->persist($OrderItems);
                $entityManager->flush();
            }

            $orders = $TempOrderRepository->findBy(
                array("sess_id" => $this->session->get('order_sess_id'))
            );
            foreach ($orders as $order) {
                $ordeId = $order->getId();
                $delete = $this->getDoctrine()->getRepository(TempOrder::class)->find($ordeId);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($delete);
                $entityManager->flush();
            }

            $this->session->invalidate();
            return $this->redirectToRoute('order_done');
        }

        return $this->render('order/customer.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/sale", name="order_sale", methods={"GET"})
     */
    public function sale(PizzaOfTheDayRepository $PizzaOfTheDayRepository, PizzaRepository $PizzaRepository): Response
    {
        $sale = $PizzaOfTheDayRepository->findOneByToday();
        $pizzaId =  $sale->getPizzaId();
        $salePrice = $sale->getPrice();

        $pizza = $PizzaRepository->find($pizzaId);

        return $this->render('order/sale.html.twig', [
            'pizza' => $pizza,
            'salePrice' => $salePrice
        ]);
    }

    /**
     * @Route("/done", name="order_done", methods={"GET"})
     */
    public function done(): Response
    {
        return $this->render('order/done.html.twig');
    }

    // /**
    //  * @Route("/delete_all", name="order_delete_all", methods={"GET"})
    //  */
    // public function deleteAllTempOrder(TempOrderRepository $TempOrderRepository)
    // {
    //     $orders = $TempOrderRepository->findBy(
    //         array("sess_id" => $this->session->get('order_sess_id'))
    //     );

    //     foreach ($orders as $order) {
    //         $ordeId = $order->getId();
    //         $delete = $this->getDoctrine()->getRepository(TempOrder::class)->find($ordeId);

    //         $entityManager = $this->getDoctrine()->getManager();
    //         $entityManager->remove($delete);
    //         $entityManager->flush();
    //     }

    //     //return $this->redirectToRoute('order_done');
    // }
}
