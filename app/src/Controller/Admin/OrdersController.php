<?php

namespace App\Controller\Admin;

use App\Entity\Order;
use App\Repository\OrderRepository;

use App\Repository\CustomerRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @Route("/admin")
 */
class OrdersController extends AbstractController
{

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/orders", name="/orders_index", methods={"GET"})
     */
    public function index(CustomerRepository $CustomerRepository): Response
    {

        if (!$this->session->get('adminLogin') || $this->session->get('adminLogin') == 'No') {
            return $this->redirectToRoute('admin_index');
        }

        $customers = $CustomerRepository->findAll();

        return $this->render('admin/orders/index.html.twig', [
            'customers' => $customers
        ]);
    }

    /**
     * @Route("/orders/{id}/view", name="order_view", methods={"GET"})
     */
    public function view(Request $request, OrderRepository $OrderRepository, CustomerRepository $CustomerRepository): Response
    {
        if (!$this->session->get('adminLogin') || $this->session->get('adminLogin') == 'No') {
            return $this->redirectToRoute('admin_index');
        }

        $customerId = $request->get("id");
        $order = $OrderRepository->findOneBy(array('Customer_ID' => $customerId));

        return $this->render('admin/orders/view.html.twig', [
            'order' => $order
        ]);
    }

    /**
     * @Route("/orders/{id}/delete", name="order_delete", methods={"GET"})
     */
    public function delete(Request $request, OrderRepository $OrderRepository, CustomerRepository $CustomerRepository): Response
    {
        if (!$this->session->get('adminLogin') || $this->session->get('adminLogin') == 'No') {
            return $this->redirectToRoute('admin_index');
        }

        $orderId = $request->get('id');

        $order = $OrderRepository->find($orderId);
        $customerId = $order->getCustomerID();
        $customer = $CustomerRepository->find($customerId);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($order);
        $entityManager->remove($customer);
        $entityManager->flush();

        return $this->redirectToRoute('/orders_index');
    }
}
