<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @Route("/admin")
 */
class IndexController extends AbstractController
{

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="admin_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    /**
     * @Route("/login", name="admin_login", methods={"POST"})
     */
    public function login(Request $request): Response
    {
        $password = $request->request->get('password');
        if ($password == '1234') {
            $this->session->set('adminLogin', 'Yes');
            return $this->redirectToRoute('/orders_index');
        }

        return $this->redirectToRoute('admin_index');
    }
}
