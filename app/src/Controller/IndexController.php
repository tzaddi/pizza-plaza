<?php

namespace App\Controller;

use App\Entity\PizzaOfTheDay;
use App\Repository\PizzaOfTheDayRepository;

use App\Repository\PizzaRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class IndexController extends AbstractController
{
    /**
     * @Route("/", name="home_index", methods={"GET"})
     */
    public function index(PizzaOfTheDayRepository $PizzaOfTheDayRepository, PizzaRepository $PizzaRepository): Response
    {
        $checkSale = $PizzaOfTheDayRepository->findOneByToday();
        if ($checkSale == NULL) {
            $pizzas = $PizzaRepository->findAll();
            shuffle($pizzas);
            $ctr = 1;
            foreach ($pizzas as $pizza) {
                $pizzaId = $pizza->getId();
                $pizzaPrice = $pizza->getPrice();
                if ($ctr == 1) {
                    break;
                }
            }

            $inPrice = $pizzaPrice * .33;
            $salePrice = $pizzaPrice - $inPrice;

            $entityManager = $this->getDoctrine()->getManager();
            $saleToday = new PizzaOfTheDay();
            $saleToday->setPizzaId($pizzaId);
            $saleToday->setPrice(number_format($salePrice, 2));
            $saleToday->setTimestamp(new \DateTime());
            $entityManager->persist($saleToday);
            $entityManager->flush();
        }

        $sale = $PizzaOfTheDayRepository->findOneByToday();
        $pizzaId =  $sale->getPizzaId();
        $salePrice = $sale->getPrice();
        $pizza_sale = $PizzaRepository->find($pizzaId);

        return $this->render('index.html.twig', [
            'pizza_sale' => $pizza_sale, 'sale_price' => $salePrice
        ]);
    }
}
