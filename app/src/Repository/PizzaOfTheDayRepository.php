<?php

namespace App\Repository;

use App\Entity\PizzaOfTheDay;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PizzaOfTheDay|null find($id, $lockMode = null, $lockVersion = null)
 * @method PizzaOfTheDay|null findOneBy(array $criteria, array $orderBy = null)
 * @method PizzaOfTheDay[]    findAll()
 * @method PizzaOfTheDay[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PizzaOfTheDayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PizzaOfTheDay::class);
    }

    // /**
    //  * @return PizzaOfTheDay[] Returns an array of PizzaOfTheDay objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PizzaOfTheDay
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findOneByToday()
    {
        $startDate = date('Y-m-d 00:00:01');
        $endDate = date('Y-m-d 23:59:59');

        $query = $this->createQueryBuilder('p')
            ->andWhere('p.timestamp >= :startDate')
            ->setParameter('startDate', $startDate)
            ->andWhere('p.timestamp <= :endDate')
            ->setParameter('endDate', $endDate)
            ->getQuery();
        return $query->getOneOrNullResult();
    }
}
