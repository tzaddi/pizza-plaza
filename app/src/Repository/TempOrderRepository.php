<?php

namespace App\Repository;

use App\Entity\TempOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TempOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method TempOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method TempOrder[]    findAll()
 * @method TempOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TempOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TempOrder::class);
    }

    // /**
    //  * @return TempOrder[] Returns an array of TempOrder objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TempOrder
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getAllOrderTemp($sess_id): ?TempOrder
    {
        return $this->createQueryBuilder('t')
            ->Where('t.sess_id = :sess_id')
            ->setParameter('sess_id', $sess_id)
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
