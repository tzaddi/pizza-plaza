<?php

namespace App\Entity;

use App\Repository\PizzaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PizzaRepository::class)
 */
class Pizza
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=OrderItems::class, mappedBy="pizza")
     */
    private $orderitems_pizza;

    public function __construct()
    {
        $this->orderitems_pizza = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|OrderItems[]
     */
    public function getOrderitemsPizza(): Collection
    {
        return $this->orderitems_pizza;
    }

    public function addOrderitemsPizza(OrderItems $orderitemsPizza): self
    {
        if (!$this->orderitems_pizza->contains($orderitemsPizza)) {
            $this->orderitems_pizza[] = $orderitemsPizza;
            $orderitemsPizza->setPizza($this);
        }

        return $this;
    }

    public function removeOrderitemsPizza(OrderItems $orderitemsPizza): self
    {
        if ($this->orderitems_pizza->removeElement($orderitemsPizza)) {
            // set the owning side to null (unless already changed)
            if ($orderitemsPizza->getPizza() === $this) {
                $orderitemsPizza->setPizza(null);
            }
        }

        return $this;
    }
}
