<?php

namespace App\Entity;

use App\Repository\OrderItemsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderItemsRepository::class)
 */
class OrderItems
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $Order_ID;

    /**
     * @ORM\Column(type="integer")
     */
    private $Pizza_ID;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderitems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $orderitems_order;

    /**
     * @ORM\ManyToOne(targetEntity=Pizza::class, inversedBy="orderitems_pizza")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pizza;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getOrderID(): ?int
    {
        return $this->Order_ID;
    }

    public function setOrderID(int $Order_ID): self
    {
        $this->Order_ID = $Order_ID;

        return $this;
    }

    public function getPizzaID(): ?int
    {
        return $this->Pizza_ID;
    }

    public function setPizzaID(int $Pizza_ID): self
    {
        $this->Pizza_ID = $Pizza_ID;

        return $this;
    }

    public function getOrderitemsOrder(): ?Order
    {
        return $this->orderitems_order;
    }

    public function setOrderitemsOrder(?Order $orderitems_order): self
    {
        $this->orderitems_order = $orderitems_order;

        return $this;
    }

    public function getPizza(): ?Pizza
    {
        return $this->pizza;
    }

    public function setPizza(?Pizza $pizza): self
    {
        $this->pizza = $pizza;

        return $this;
    }
}
